var mongoose      = require("mongoose"),
	faker 		  = require('faker'),
    Campground    = require("./models/campground"),
    Comment       = require("./models/comment")



var data = [];
for(var i=0; i < 16; i++) {
    var obj = {
        name: faker.name.firstName(),
        image: faker.image.imageUrl(),
        description: faker.lorem.paragraph()
    }
    data.push(obj);
}


function seedDB(){
	Campground.remove({}, function(err) {
		if(err) {
			console.log(err);
		}
		console.log("removed all entries");	

			data.forEach(function(seed){
			Campground.create(seed, function(err, campground){
				if(err) {
					console.log(err);
				} else {
					console.log("added some campgrounds");
					Comment.create(
						{
							text: "This is a great place",
							author: "Homer"
						}, function(err, comment) {
							if(err) {
								console.log(err);
							} else {
							campground.comments.push(comment._id);
							campground.save();
							console.log("Newcomment");
						}
					});
				}
			});
		});
	});

	
}

module.exports = seedDB;



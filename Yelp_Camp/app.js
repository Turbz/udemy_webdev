var express       = require('express'),
    request       = require("request"),
    app           = express(),
    bodyParser    = require("body-parser"),
    mongoose      = require("mongoose"),
    seedDB        = require("./seeds")
    //Models
    Campground    = require("./models/campground")
    Comment       = require("./models/comment"),
    // User          = require("./models/user")

seedDB();
//mongoose.connect("mongodb://localhost/yelpCamp");
mongoose.connect("mongodb://eratiosu:Akaishu23!@ds251827.mlab.com:51827/yelpcamp");
app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");


app.get('/', function (req,res) {
	res.render("home");
});





//INDEX - show all campgrounds
app.get('/campgrounds', function (req,res) {
  Campground.find({}, function(err, allCampgrounds){
      if(err){
      console.log(err);
    } else {
      res.render("index", {campgrounds:allCampgrounds});
    }

  });
  
});




//CREATE - add new campground to db
app.post("/campgrounds", function(req,res){
  var newCampname = req.body.name;
  var newCampimage = req.body.image;
  var newCampDescription = req.body.description;
  var newCampground = {name: newCampname, image: newCampimage, description: newCampDescription};
  Campground.create(newCampground, function(err, newlyCreated){
      if(err){
        console.log(err);
    } else {
      res.redirect("/campgrounds");
    }

  });
});




//NEW - Show form to create new campground and add to db
app.get('/campgrounds/new', function (req,res) {
  res.render("new");
});


//Show - shows more info about a campground
app.get('/campgrounds/:id', function (req,res) {
  Campground.findById(req.params.id).populate("comments").exec(function(err, foundCampground) {
      if(err){
        console.log(err);
    } else {
      console.log(foundCampground);
        res.render("show", {campground: foundCampground});
    }
  });
});






app.listen(3000, function () {
  console.log('YelpCamp has started on port 3000!');
});

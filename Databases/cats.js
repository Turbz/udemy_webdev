const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost/cat_app');
 
 const catSchema = new mongoose.Schema({
	name: String,
	age: Number,
	temperament: String
	
});


const Cat = mongoose.model('Cat', catSchema);
// export the model, so we can use it everywhere
module.exports = Cat;

var buba = new Cat({
	name: "Buba",
	age: 12,
	temperament: "Grouchy"
});

buba.save(function(err, cat){
	if(err) {
		console.log("Went wriong")
	} else {
		console.log("Saved the entry to database")
		console.log(buba);
	}
});
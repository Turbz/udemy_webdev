//Require all packages
var express = require('express'),
    app = express(),
    bodyParser = require("body-parser"),
    methodOverride = require("method-override"),
    mongoose = require("mongoose");

//Connect to database
mongoose.connect("mongodb://localhost/Blog");

//App settings
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride("_method"));
app.set("view engine", "ejs");
app.use(express.static("public"));



//MongoDB schema set up
var blogSchema = new mongoose.Schema({
  title: String,
  image: String,
  body: String,
  created: {type: Date, default: Date.now}
});

//Instantite Schema
var Blog = mongoose.model("Blog", blogSchema)


//RESTful routes
app.get('/', function (req, res) {
  res.redirect("/blogs");
});


//RESTful routes
app.get('/blogs', function (req, res) {
  Blog.find({}, function(err, blogs){
    if(err) {
      console.log("ERRORROROROROROR!!!")
    } else {
       res.render("index", {blogs: blogs});
    }
  });	
});



// New Route
app.get('/blogs/new', function(req, res) {
  res.render("new");
});


// Make the create route
app.post('/blogs', function(req, res) {
  Blog.create(req.body.blog, function(err, newBlog){
      if(err) {
          res.render("new");
      } else {
         res.redirect("/blogs");
      }
  });
  
});



//Show route
app.get('/blogs/:id', function(req, res) {
  Blog.findById(req.params.id, function(err, foundBlog) {
    if(err) {
        res.redirect("/blogs");
      } else {
        res.render("show", {blog: foundBlog});
      }
  });
});


//Edit route
app.get('/blogs/:id/edit', function(req, res) {
  Blog.findById(req.params.id, function(err, editBlog) {
    if(err) {
        res.redirect("/blogs");
      } else {
        res.render("edit", {blog: editBlog});
      }
  });
});

//Update route
app.put('/blogs/:id', function(req, res) {
  Blog.findByIdAndUpdate(req.params.id, req.body.blog, function(err, updatedBlog) {
    if(err) {
        res.redirect("/blogs");
      } else {
        res.redirect("/blogs/" + req.params.id);
      }
  });
});


//Delete route
app.delete('/blogs/:id', function(req, res) {
  Blog.findByIdAndRemove(req.params.id, function(err) {
    if(err) {
        res.redirect("/blogs");
      } else {
        res.redirect("/blogs");
      }
  });
});




app.listen(3000, function () {
  console.log('Blog is up and running!');
});
